﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallSystem;

namespace RingSystem
{
    public interface IRingService
    {
        void AddToBallDictonary(BallController ballController);
    }
}
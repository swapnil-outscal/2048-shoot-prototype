﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallSystem;
using InputSystem;

namespace RingSystem
{
    public class RingService : MonoBehaviour, IRingService
    {
        public InputService inputService;
        public RingController ringController;
        public BallService ballService;

        void Start()
        {
            ringController.SetRingService(this); 
        }

        public void AddToBallDictonary(BallController ballController)
        {
            ringController.AddToBallDictonary(ballController);
        }

        public void AddToBallDictonary(BallController ballController, float angle)
        {
            ringController.AddToBallDictonary(ballController, angle);
        }
    }
}
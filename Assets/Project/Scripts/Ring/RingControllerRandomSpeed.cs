﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RingSystem
{
    public class RingControllerRandomSpeed : RingController
    {
        //[SerializeField]
        //private float speedIncrease;
        //private float lastSpeed, newSpeed;

        private float timer = 4;

        protected override void Update()
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
                base.Update();
            }
            else
                SelectRotationSpeed();
        }
        protected override void RotateRing()
        {
            base.RotateRing();
        }

        private float SelectRotationSpeed()
        {
            timer = Random.Range(3, 10);
            //lastSpeed = rotatingSpeed;
            return rotatingSpeed = Random.Range(30, 90);
        }

        //async void SpeedProgress()
        //{
        //    if((newSpeed - rotatingSpeed) > 0)
        //    {
        //        newSpeed += Time.deltaTime * speedIncrease;
        //    }
        //}


    }
}
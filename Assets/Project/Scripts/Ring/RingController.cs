﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallSystem;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.IO;

namespace RingSystem
{
    public class RingController : MonoBehaviour
    {
        public float rotatingSpeed;
        [SerializeField] private SpriteRenderer ringSprite;
        private SortedDictionary<float, BallController> ballDictionary;

        private int latestBallIndex;
        private List<float> angles;
        private RingService ringService;

        private bool merging = false;
        [SerializeField]
        private float radius = 2;

        private int count = 0;
        private int yoInt = 0;

        // Start is called before the first frame update
        void Start()
        {
            angles = new List<float>();
            ballDictionary = new SortedDictionary<float, BallController>();
        }

        public void SetRingService(RingService ringService)
        {
            this.ringService = ringService; 
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            RotateRing();
        }

        protected virtual void RotateRing()
        {
            transform.Rotate(Vector3.forward, rotatingSpeed * Time.deltaTime);
        }

        public void AddToBallDictonary(BallController ballController)
        {
            float keyAngle = 270 - transform.localEulerAngles.z;
            keyAngle = keyAngle % 360;
            if (keyAngle < 0)
            {
                keyAngle = 360 + keyAngle;
            }
            Debug.Log("<color=blue>KeyAngle</color>" + keyAngle);
            if (keyAngle > 360)
            {
                Time.timeScale = 0;
            }
            ballDictionary.Add(keyAngle, ballController);
            latestBallIndex = Array.IndexOf(ballDictionary.Keys.ToArray(), keyAngle);

       
            ringService.inputService.CanShoot(false);
            CanMerge(latestBallIndex);
        }

        public void AddToBallDictonary(BallController ballController, float angle)
        {
            Debug.Log("*****************" + angle);
            ballDictionary.Add(angle, ballController);
            latestBallIndex = Array.IndexOf(ballDictionary.Keys.ToArray(), angle);
            ringService.inputService.CanShoot(false);
            CanMerge(latestBallIndex);
        }

        async void CanMerge(int index)
        {
            await new WaitForEndOfFrame();

            merging = false;
            if (ballDictionary.Count > 1)
            {
                if (index == 0)
                {
                    if (ballDictionary.ElementAt(index).Value.ballType.ballValue == ballDictionary.ElementAt(index + 1).Value.ballType.ballValue)
                    {
                        MergeBalls(index, index + 1);
                    }
                    else if (ballDictionary.ElementAt(index).Value.ballType.ballValue == ballDictionary.ElementAt(ballDictionary.Count - 1).Value.ballType.ballValue)
                    {
                        MergeBalls(index, ballDictionary.Count - 1);
                    } 
                }
                else if (index == ballDictionary.Count - 1)
                {
                    if (ballDictionary.ElementAt(index).Value.ballType.ballValue == ballDictionary.ElementAt(index - 1).Value.ballType.ballValue)
                    {
                        MergeBalls(index, index - 1);
                    }
                    else if (ballDictionary.ElementAt(index).Value.ballType.ballValue == ballDictionary.ElementAt(0).Value.ballType.ballValue)
                    {
                        MergeBalls(index, 0);
                    }
                    
                }
                else if (index > 0 && index < ballDictionary.Count - 1)
                {
                    if (ballDictionary.ElementAt(index).Value.ballType.ballValue == ballDictionary.ElementAt(index + 1).Value.ballType.ballValue)
                    {
                        MergeBalls(index, index + 1);
                    }
                    else if (ballDictionary.ElementAt(index).Value.ballType.ballValue == ballDictionary.ElementAt(index - 1).Value.ballType.ballValue)
                    {
                        MergeBalls(index, index - 1);
                    }
                }
            }

            if (merging == false)
                ringService.inputService.CanShoot(true);
        }


        async void MergeBalls(int firstIndex, int secondIndex)
        {
            //Debug.ClearDeveloperConsole();
            merging = true;
            DictonaryDebug();
           // ScreenCapture.CaptureScreenshot("Merge" + "_" + yoInt++);
            await new WaitForSeconds(0.5f);

            float finalAngle = 0;
            float smallerAngel;
            float angleOne, angleTwo, angleDiff = 0;

            angleOne = ballDictionary.ElementAt(firstIndex).Key;
            angleTwo = ballDictionary.ElementAt(secondIndex).Key;

            if (firstIndex < secondIndex)
            {
                smallerAngel = angleOne;
            }
            else
            {
                smallerAngel = angleTwo;
            }

            angleDiff =  (Mathf.Abs(angleOne - angleTwo) / 2);

            if (Math.Abs(secondIndex - firstIndex) == 1)
            {
                finalAngle = smallerAngel + angleDiff;
            }
            else
            {
                finalAngle = 180 + angleDiff + smallerAngel;
            }

            float localFinalAngle = finalAngle;
            finalAngle += transform.eulerAngles.z;

            finalAngle = finalAngle % 360;
            localFinalAngle = localFinalAngle % 360;
            Vector3 position = Vector3.zero;
            position.x =  Mathf.Cos(finalAngle * Mathf.Deg2Rad);
            position.y =  Mathf.Sin(finalAngle * Mathf.Deg2Rad);
            
            BallController firstBall = ballDictionary[angleOne];
            //BallController secondBall = ballDictionary[angleTwo];

            Destroy(ballDictionary.ElementAt(firstIndex).Value.gameObject);
            Destroy(ballDictionary.ElementAt(secondIndex).Value.gameObject);
            //ballDictionary.ElementAt(firstIndex).Value.Merge(angle);
            //ballDictionary.ElementAt(secondIndex).Value.Merge(angle);
            ballDictionary.Remove(angleOne);
            ballDictionary.Remove(angleTwo);

            ringService.ballService.MergeBalls(firstBall.ballType, position * radius, this.transform, localFinalAngle);



            DictonaryDebug();
            //ScreenCapture.CaptureScreenshot("Merge" + "_" + yoInt++);
        }

        void DemoBall()
        {
            Vector3 position = Vector3.zero;
            position.x = Mathf.Cos(0 * Mathf.Deg2Rad);
            position.y = Mathf.Sin(0 * Mathf.Deg2Rad);

            ringService.ballService.ballSpawner.SpawnBall(position, transform, 0);
        }

        void DictonaryDebug()
        {
            Debug.Log("<color=red> ***************************************** </color>");
            foreach (var key in ballDictionary.Keys)
            {
                Debug.Log("<color=green>[RingController] Key:</color>" + key
                + " Value:" + ballDictionary[key].ballType.ballValue);
            }
        }

        void DebugInfo(int firstIndex, int secondIndex)
        {
            Debug.Log("[RingController] FirstIndex:" + firstIndex + " SecondIndex:" + secondIndex);
            //Debug.Log("[RingController] FirstValue:" + ballDictionary.ElementAt(firstIndex).Value.ballType.ballValue
            //+ " SecondValue:" + ballDictionary.ElementAt(secondIndex).Value.ballType.ballValue);
        }
    }
}
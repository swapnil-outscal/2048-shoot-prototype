﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallSystem
{
    public class BallSpawnerRandomBlock : BallSpawner
    {
        private void Start()
        {
            nextBallType = ballTypes[0];
            spawnSprite.sprite = nextBallType.ballSprite;
        }

        public override void SpawnBall()
        {
            GameObject ball = Instantiate(ballControllerPrefab.gameObject);
            ball.transform.position = transform.position;
            ball.GetComponent<BallController>().SetBallType(nextBallType);
            ball.GetComponent<BallController>().SetBallService(ballService);
            ball.GetComponent<BallController>().Shoot();

            nextBallType = ballTypes[Random.Range(0, 3)];
            spawnSprite.sprite = nextBallType.ballSprite;
        }

    }
}
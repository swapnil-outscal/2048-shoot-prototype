﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RingSystem;
using UISystem;

namespace BallSystem
{
    public class BallService : MonoBehaviour, IBallService
    {
        public RingService ringService;

        public BallSpawner ballSpawner;

        public void Spawnball()
        {
            ballSpawner.SetBallService(this);
            ballSpawner.SpawnBall();
        }

        public void AddBallToRing(BallController ballController)
        {
            ringService.AddToBallDictonary(ballController);
        }

        public void AddBallToRing(BallController ballController, float angle)
        {
            ringService.AddToBallDictonary(ballController, angle);
        }

        public void MergeBalls(BallType ballType, Vector3 position, Transform parent, float angle)
        {
            ballSpawner.MergeBalls(ballType, position, parent, angle);
        }

        public void GameOver()
        {
            UIService.instance.GameOver();        
        }

    }
}
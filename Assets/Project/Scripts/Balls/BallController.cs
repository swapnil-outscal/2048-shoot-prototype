﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RingSystem;
using TMPro;

namespace BallSystem
{
    public class BallController : MonoBehaviour
    {
        [SerializeField] private float shootForce;
        private BallService ballService;

        [SerializeField] private SpriteRenderer ballSprite;

        [SerializeField] private Rigidbody2D myBody;

        private bool inShoot = false;
        private bool detectTrigger = false;

        public BallType ballType;//, higherBallType;

        void Awake()
        {
            detectTrigger = false;
            myBody.isKinematic = false;
            myBody.gravityScale = 0;
        }

        public void SetBallService(BallService ballService)
        {
            this.ballService = ballService;
        }

        public void SetBallType(BallType ballType)
        {
            this.ballType = ballType;
            ballSprite.sprite = ballType.ballSprite;
        }

        public void Shoot()
        {
            detectTrigger = true;
            inShoot = true;
            myBody.AddForce(Vector2.up * shootForce, ForceMode2D.Impulse);
        }

        public void Merge(float finalAngle)
        {
            //iTween.MoveTo(gameObject,{"x":3,"time":4,"delay":1,"onupdate","myUpdateFunction","looptype","pingpong"});             

            Hashtable hashtable = new Hashtable();
            hashtable.Add("time",0.5f);
            hashtable.Add("oncomplete", "DestroyObj");
            hashtable.Add("rotation", new Vector3(0, 0, finalAngle));
            hashtable.Add("islocal", true);
            iTween.RotateTo(this.gameObject, hashtable);
        }


        async void Rotate()
        {
             
        }

        private void DestroyObj()
        {
            Destroy(gameObject); 
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (detectTrigger)
            {
                if (collision.GetComponent<RingController>() != null)
                {
                    inShoot = false;
                    myBody.velocity = Vector3.zero;
                    myBody.transform.SetParent(collision.transform);
                    myBody.isKinematic = true;
                    ballService.AddBallToRing(this);
                }
                else if (collision.GetComponent<BallController>() != null)
                {
                    if (inShoot)
                    {
                        ballService.GameOver();
                        myBody.velocity = Vector3.zero;
                        myBody.transform.SetParent(collision.transform);
                        myBody.isKinematic = true;
                        Destroy(this);
                    }
                }
            }
        }
    }
}
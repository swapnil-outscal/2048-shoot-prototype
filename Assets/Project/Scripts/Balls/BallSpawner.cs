﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallSystem
{
    [System.Serializable]
    public struct BallType
    {
        public int ballValue;
        public Sprite ballSprite; 
    }

    public class BallSpawner : MonoBehaviour
    {
        public List<BallType> ballTypes;
        public BallController ballControllerPrefab;

        [SerializeField]
        protected SpriteRenderer spawnSprite;

        protected BallService ballService;
        protected BallType nextBallType;

        private void Start()
        {
            nextBallType = ballTypes[0];
            spawnSprite.sprite = nextBallType.ballSprite;
        }

        public void SetBallService(BallService ballService)
        {
            this.ballService = ballService;
        }

        public virtual void SpawnBall()
        {
            GameObject ball = Instantiate(ballControllerPrefab.gameObject);
            ball.transform.position = transform.position;
            ball.GetComponent<BallController>().SetBallType(nextBallType);
            ball.GetComponent<BallController>().SetBallService(ballService);
            ball.GetComponent<BallController>().Shoot();

            nextBallType = ballTypes[0];
            spawnSprite.sprite = nextBallType.ballSprite;
        }

        public void SpawnBall(Vector3 position, Transform parent, int typeIndex)
        {
            nextBallType = ballTypes[typeIndex];

            GameObject ball = Instantiate(ballControllerPrefab.gameObject);
            ball.transform.SetParent(parent);
            ball.transform.position = parent.position + position;
            ball.GetComponent<BallController>().SetBallType(nextBallType);
            ball.GetComponent<BallController>().SetBallService(ballService);
            //ball.GetComponent<BallController>().Shoot();

            spawnSprite.sprite = nextBallType.ballSprite;
        }

        public void MergeBalls(BallType ballType, Vector3 spawnPosition, Transform parent, float angle)
        {
            BallType newBallType = new BallType();
            for (int i = 0; i < ballTypes.Count; i++)
            {
                if(ballType.ballValue == ballTypes[i].ballValue)
                {
                    int ballTypeIndex = i + 1;
                    if (ballTypeIndex < ballTypes.Count)
                    {
                        newBallType = ballTypes[ballTypeIndex];
                        GameObject ball = Instantiate(ballControllerPrefab.gameObject, spawnPosition, Quaternion.identity);
                        ball.transform.SetParent(parent);
                        ball.transform.position = parent.position + spawnPosition;
                        ball.transform.localRotation = Quaternion.Euler(0, 0, angle+90);
                        ball.GetComponent<BallController>().SetBallType(newBallType);
                        ball.GetComponent<BallController>().SetBallService(ballService);
                        ballService.ringService.AddToBallDictonary(ball.GetComponent<BallController>(), angle);
                    }
                    else
                    {
                        //Game Won 
                        Debug.Log("Game Won");
                    }
                }
            }
        }

    }
}
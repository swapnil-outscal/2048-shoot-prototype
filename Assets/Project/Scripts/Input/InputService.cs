﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallSystem;
using UISystem;

namespace InputSystem
{
    public class InputService : MonoBehaviour, IInputService
    {
        public BallService ballService;

        public InputController inputController;

        void Start()
        {
            UIService.instance.SetInputService(this);
        }

        public void CanShoot(bool value)
        {
            inputController.CanShoot(value); 
        }
    }
}
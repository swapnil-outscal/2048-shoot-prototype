﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InputSystem
{
    public class InputController : MonoBehaviour
    {
        private bool canShoot = true;
        [SerializeField]
        private InputService inputService;

        public void CanShoot(bool value)
        {
            canShoot = value;
        }

        private void Update()
        {
            if(Input.GetMouseButtonDown(0) && canShoot)
            {
                canShoot = false;
                inputService.ballService.Spawnball();
            }
        }


    }
}
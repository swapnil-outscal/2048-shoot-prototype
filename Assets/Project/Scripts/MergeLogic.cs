﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergeLogic : MonoBehaviour
{
    public int firstVal, secondVal;
    public float angleOne, angleTwo;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Merge(); 
        }
    }

    void Merge()
    {
        float angle = 0;
        float angleDiff = 0; ;
        //angleOne = ballDictionary.ElementAt(firstVal).Key;
        //angleTwo = ballDictionary.ElementAt(secondVal).Key;
        if (firstVal < secondVal)
        {
            if ((secondVal - firstVal) == 1)
            {
                Debug.Log("<color=red>[RingController] Angles </color>" + angleOne + " " + angleTwo);
                angleDiff = (angleTwo - angleOne) / 2;
                angle = angleDiff + angleOne;
                Debug.Log("<color=red>[RingController] 1st < 2nd Angle:</color> ANgle:" + angle + " AngleDiff" + angleDiff);
            }
            else
            {
                Debug.Log("<color=red>[RingController] Angles </color>" + angleOne + " " + angleTwo);
                angleDiff = (360 - Mathf.Abs(angleTwo - angleOne)) / 2;
                angle = angleDiff + angleTwo;
                Debug.Log("<color=red>[RingController] 1st < 2nd Angle:</color> ANgle:" + angle + " AngleDiff" + angleDiff);
            }

            //Debug.Log("<color=red>[RingController] 1st < 2nd Angle:</color> ANgle:" + angle + " AngleDiff" + angleDiff);
        }
        else
        {
            if ((firstVal - secondVal) == 1)
            {
                angleDiff = (angleOne - angleTwo) / 2;
                Debug.Log("<color=red>[RingController] Angles </color>" + angleOne + " " + angleTwo);
                angle = (angleTwo + angleDiff);
                Debug.Log("<color=red>[RingController] 1 - 2 == 1 Angle:</color> ANgle:" + angle + " AngleDiff" + angleDiff);
            }
            else
            {
                angleDiff = (360 - Mathf.Abs(angleOne - angleTwo)) / 2;
                Debug.Log("<color=red>[RingController] Angles </color>" + angleOne + " " + angleTwo);
                angle = angleDiff + angleOne;
                Debug.Log("<color=red>[RingController] 1 - 2 != 1 Angle:</color> ANgle:" + angle + " AngleDiff" + angleDiff);
            }

            //Debug.Log("<color=red>[RingController] Angles </color>" + angleOne + " " + angleTwo);
            //Debug.Log("<color=red>[RingController] 1st > 2nd Angle:</color> ANgle:" + angle + " AngleDiff" + angleDiff);
        }
    }
}

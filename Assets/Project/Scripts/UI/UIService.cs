﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InputSystem;

namespace UISystem
{
    public class UIService : MonoBehaviour, IUIService
    {
        private InputService inputService;
        public UIController uiController;

        public static UIService instance;

        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != null)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject); 
        }

        public void SetInputService(InputService inputService)
        {
            this.inputService = inputService;
        }

        void Start()
        {
            uiController.SetUIService(this);
        }

        public void CanShoot(bool value)
        {
            inputService.CanShoot(value); 
        }

        public void GameOver()
        {
            Debug.Log("[UIService] GameOver");
            uiController.GameOver();
            CanShoot(false);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UISystem
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private GameObject menuPanel, gameOverPanel;

        [SerializeField] private Button startBtn, retryBtn, mainMenubtn;
        [SerializeField] private Button normalGame, randomSpeed, randomBlock;

        private UIService uiService;

        public void Start()
        {
            menuPanel.SetActive(true);
            //startBtn.onClick.AddListener(() => NormalGame());
            retryBtn.onClick.AddListener(() => RetryBtn());

            normalGame.onClick.AddListener(() => NormalGame());
            randomSpeed.onClick.AddListener(() => RandomSpeedGame());

            randomBlock.onClick.AddListener(() => RandomBlockGame());
            mainMenubtn.onClick.AddListener(() => MainMenuGame());
        }

        public void SetUIService(UIService uiService)
        {
            this.uiService = uiService;
        }

        public void GameOver()
        {
            gameOverPanel.SetActive(true); 
        }

        void NormalGame()
        {
            StartBtn();
            SceneManager.LoadScene("GamePlay");
        }

        void RandomSpeedGame()
        {
            StartBtn();
            SceneManager.LoadScene("GamePlayRandomSpeed");
        }

        void RandomBlockGame()
        {
            StartBtn();
            SceneManager.LoadScene("GamePlayRandomBlocks");
        }

        void MainMenuGame()
        {
            gameOverPanel.SetActive(false);
            menuPanel.SetActive(true);
            SceneManager.LoadScene("MainMenu");
        }

        void StartBtn()
        {
            menuPanel.SetActive(false);
            //uiService.CanShoot(true);
        }

        void RetryBtn()
        {
            gameOverPanel.SetActive(false);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }
}